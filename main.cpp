//
//  main.cpp
//  sort_methods
//
//  Created by Gordiy Rushynets on 2/21/19.
//  Copyright © 2019 Gordiy Rushynets. All rights reserved.
//

#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Student
{
private:
    string name;
    string faculty;
    int age;
public:
    string get_name()
    {
        return name;
    }
    
    string get_faculty()
    {
        return faculty;
    }
    
    int get_age()
    {
        return age;
    }
    
    friend std::istream &operator>>(std::istream &in, Student &student)
    {
        return in >> student.name >> student.faculty >> student.age;
    }
    
    friend ostream &operator<<(ostream &out, Student &student)
    {
        return out << student.name << ' ' << student.faculty << ' ' << student.age;
    }
    
};

int main(int argc, const char * argv[]) {
    // reading from students.txt file information about students and filling array of students
    int count_students;
    ifstream fin("PATH TO FILE");
    
    // get count students from firs line of students.txt
    fin >> count_students;
    
    Student *students = new Student[count_students];
    
    for(int i = 0; i < count_students; ++i)
    {
        students[i] = Student();
        fin >> students[i];
    }
    
    // use bubble sort by age
    for(int i = 0; i < count_students; ++i)
    {
        for(int j = 0; j < count_students - i - 1; ++j)
        {
            if(students[j].get_age() < students[j+1].get_age())
            {
                swap(students[j], students[j+1]);
            }
        }
    }
    
    // use shell sort by name
    for(int d = count_students/2; d >= 1; d /= 2)
    {
        for(int i = d; i < count_students; ++i)
        {
            for(int j  = i; j >= d && students[j-d].get_name() > students[j].get_name(); j -= d)
            {
                swap(students[j], students[j-d]);
            }
        }
    }
    
    system("pause");
    return 0;
}
